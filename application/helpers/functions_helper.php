<?php

function encrypt_password($password,$key) 
{
    $token  = hash_hmac('sha256',$password,$key);
    return $token;
}

function IsNullOrEmptyString($str){
    return (!isset($str) || trim($str) === '');
}

function replace_html_tags($string)
{
	$arr_tags = array("<p>","</p>");
	$arr_repl = array(" ","<br />");

	$new_string = str_replace($arr_tags,$arr_repl,$string);

	return $new_string;
}

function get_nilai_baku_mutu_info()
{
	$html = '
		<div class="card card">
        	<div class="card-header">
            	<h3 class="card-title">Pentunjuk Indikator Warna</h3>
                <div class="card-tools">
                	<button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                	<i class="fas fa-minus"></i></button>
            	</div>
            </div>
            <div class="card-body" style="display: block;">
            	<p>Sesuai dengan Nilai Baku Mutu (NBM) yang sudah ditetapkan oleh pemerintah, akan memiliki indikator sebagai berikut sesuai kondisinya.</p>
                <div class="col-lg-12 row">

                	<div class="col-sm-4">
                        <div class="small-box bg-danger">
                            <div class="inner">
                                <p align="center" style="margin-top: 1rem;">Melebihi NMB</p>
                            </div>                              
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="small-box bg-warning">
                            <div class="inner">
                                <p align="center" style="margin-top: 1rem;">Mendekati Batas NMB</p>
                            </div>                              
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="small-box bg-success">
                            <div class="inner">
                                <p align="center" style="margin-top: 1rem;">Dibawah Batas NMB</p>
                            </div>                              
                        </div>
                    </div>
                
                </div>                        
            </div>                                        
        </div>
	';

	return $html;
}

function format_detail_maintenence($status,$detail_data)
{
    $form_realisation = "";
    if ($status == true)
    {
        foreach($detail_data  as $row => $value)
        {
            $cheked_yes = ''; $cheked_no = '';
            if (intval($value['value_yes']) === 1)
            {
                $cheked_yes = 'checked';
                $cheked_no = '';
            }
            else if (intval($value['value_no']) === 1)
            {
                $cheked_yes = '';
                $cheked_no = 'checked';
            }

            $form_realisation .= '
            <tr>
                <td width="5%" align="center">'.$value['order_no'].'.</td>
                <td width="35%" align="left">'.$value['description'].'</td>
                <td width="5%" align="center">
                    <input '.$cheked_yes.' class="c_ya" type="checkbox" disabled />
                </td>
                <td width="5%" align="center">
                    <input '.$cheked_no.' class="c_tidak" type="checkbox" disabled />
                </td>
                <td width="20%" align="left">'.$value['value_note'].'</td>
                <td width="30%" align="left">'.$value['remarks'].'</td>
            </tr>';
        }
    }
    else
    {
        $form_realisation = '<tr><td align="center" colspan="6">No data available in table</td></tr>';
    }
    
    $return_detail = '
        <table border="1" style="border-collapse: collapse;width:100%;">
            <thead>
                <tr>
                    <th rowspan="2"><div align="center">No.</div></th>
                    <th rowspan="2"><div align="left">Kegiatan Reguler</div></th>
                    <th colspan="2"><div align="center">Status</div></th>
                    <th rowspan="2"><div align="left">Catatan</div></th>
                    <th rowspan="2"><div align="left">Remark</div></th>                                
                </tr>
                <tr>
                    <th><div align="center">Ya</div></th>
                    <th><div align="center">Tidak</div></th>                              
                </tr>
            </thead>
            <tbody>
                '.$form_realisation.'
            </tbody>
        </table>
    ';

    return $return_detail;
}