<?php

define('API_URL','http://localhost:8114/envipro/api/v2/');
define('DEVICE_TYPE','WEB');
define('VERSION','V.1.0.1');
define('SECRET_KEY','ENVIPROQPS2021');

define("ERROR_WRONG_PARAM","Attention, received not appropriate parameters. Please call administrator system.");
define("ERROR_NETWORK","Attention, There was a problem on network. please contact administrator system.");
define("ERROR_WRONG_EMPTY_DATA","Attention, You do not have the data need.");
define("ERROR_DATA_NOT_FOUND","Attention, Data not found. please contact administrator system.");
define("ERROR_PROCESS","Attention, There was a problem on process data. please contact administrator system.");
define("ERROR_KEYS","Attention, API keys data not found. Please contact administrator system.");
define("ERROR_GLOBAL","Attention, Something wrong. Please contact administrator system.");
define("ERROR_ALREADY_REGISTER", "Attention, you have already register this data.");

define("EMPTY_LIST", "Empty data list.");

define("ERROR_DISCONTINUE","Attention, This link has been discontinue.");

define("CONTINUE_APPROVAL","Approval will continue to the next step");
define("FINAL_APPROVAL","Approval process already finished. With status : ");

define("ERROR_LOGIN","Attention, Login Failed. Please use valid username and password.");
define("ERROR_USER_ALREADY_USED","Attention, Data user name already used, Please use another username for register new user.");
define("ERROR_MAINTENENCE_ALREADY_REGISTER", "Attention, you have already create maintenence data.");

define("ERROR_SN_ALREADY_EXIST","Attention, Serial No. already exists. Please use another serial no.");
define("ERROR_CALLSIGN_ALREADY_EXIST","Attention, Callsign already exists. Please use another callsign.");

define("ERROR_DEVICE_ALREADY_REGISTER","Attention, Data Device already registered. Your package is BASIC, please upgrade package first.");
define("ERROR_USER_ALREADY_REGISTER","Attention, Data User already registered. Your package is BASIC, please upgrade package first.");

define('ERROR_NO_DATA_AVAILABLE', "No data available in table");
define("ERROR_ONLY_THIRTY","Attention,You can only generate data for the past 30 days. Your package is BASIC, please upgrade package first.");

define("MAX_2_MB", 2097152);
define('ERROR_MAX_FILE_2_MB', "Attention, you can only select file upload under size 2 MB.");

?>