<!-- Required Jquery -->
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery-ui/jquery-ui.min.js "></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/popper.js/popper.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap/js/bootstrap.min.js "></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/pages/widget/excanvas.js "></script>
<script src="<?php echo base_url();?>assets/datatables/datatables/datatables.min.js"></script>
<script src="<?php echo base_url();?>assets/datatables/datatables/datatables.script.min.js"></script>

<body>
	<!-- Pre-loader start -->
	<div class="theme-loader">
	  <div class="loader-track">
	      <div class="preloader-wrapper">
	          <div class="spinner-layer spinner-blue">
	              <div class="circle-clipper left">
	                  <div class="circle"></div>
	              </div>
	              <div class="gap-patch">
	                  <div class="circle"></div>
	              </div>
	              <div class="circle-clipper right">
	                  <div class="circle"></div>
	              </div>
	          </div>
	          <div class="spinner-layer spinner-red">
	              <div class="circle-clipper left">
	                  <div class="circle"></div>
	              </div>
	              <div class="gap-patch">
	                  <div class="circle"></div>
	              </div>
	              <div class="circle-clipper right">
	                  <div class="circle"></div>
	              </div>
	          </div>
	        
	          <div class="spinner-layer spinner-yellow">
	              <div class="circle-clipper left">
	                  <div class="circle"></div>
	              </div>
	              <div class="gap-patch">
	                  <div class="circle"></div>
	              </div>
	              <div class="circle-clipper right">
	                  <div class="circle"></div>
	              </div>
	          </div>
	        
	          <div class="spinner-layer spinner-green">
	              <div class="circle-clipper left">
	                  <div class="circle"></div>
	              </div>
	              <div class="gap-patch">
	                  <div class="circle"></div>
	              </div>
	              <div class="circle-clipper right">
	                  <div class="circle"></div>
	              </div>
	          </div>
	      </div>
	  </div>
	</div>
	<!-- Pre-loader end -->

	<div id="pcoded" class="pcoded">
		<div class="pcoded-overlay-box"></div>
		<nav class="navbar header-navbar pcoded-header">
			<?php $this->load->view('template/header_area'); ?>
		</nav>
		<div class="pcoded-main-container">
			<div class="pcoded-wrapper">
				<div class="pcoded-content">
					<?php $this->load->view($isi); ?>
				</div>
			</div>
		</div>
	</div>