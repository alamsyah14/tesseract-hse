<div class="navbar-wrapper">
	<div class="navbar-logo">
		<a href="<?php echo base_url();?>"><img class="img-fluid" src="<?php echo base_url();?>assets/images/hse-logo-white.png" alt="Theme-Logo" /></a>
		<a class="mobile-options waves-effect waves-light"><i class="ti-more"></i></a>
	</div>
	<div class="navbar-container container-fluid">
		<ul class="nav-left">
			<li class="header-notification">
				<a href="#!" class="waves-effect waves-light">
					Modul HSE
					<i class="ti-angle-down"></i>
				</a>
				<ul class="show-notification">
					<li class="waves-effect waves-light"><a href="<?php echo base_url().'modul/registerPerbaikan'; ?>"><i class="ti-file"></i> Register Tindakan Perbaikan </a></li>
					<li class="waves-effect waves-light"><a href="<?php echo base_url().'modul/hiradc'; ?>"><i class="ti-file"></i> HIRADC </a></li>
					<li class="waves-effect waves-light"><a href="<?php echo base_url().'modul/reportHazard'; ?>"><i class="ti-files"></i> Hazard Report </a></li>
					<li class="waves-effect waves-light"><a href="#"><i class="ti-file"></i> SHE Committee </a></li>
					<li class="waves-effect waves-light"><a href="#"><i class="ti-file"></i> SHE Meeting </a></li>
					<li class="waves-effect waves-light"><a href="#"><i class="ti-file"></i> Legal Compliance </a></li>
					<li class="waves-effect waves-light"><a href="#"><i class="ti-file"></i> Accident Report </a></li>
					<li class="waves-effect waves-light"><a href="#"><i class="ti-file"></i> Inisiatif K3L </a></li>
					<li class="waves-effect waves-light"><a href="#"><i class="ti-file"></i> SHE Training </a></li>
					<li class="waves-effect waves-light"><a href="#"><i class="ti-file"></i> Pengolahan Limbah B3 </a></li>
					<li class="waves-effect waves-light"><a href="#"><i class="ti-file"></i> Audit SMK3L </a></li>
					<li class="waves-effect waves-light"><a href="#"><i class="ti-file"></i> Management Review </a></li>
				</ul>
			</li>
			<li class="header-notification">
				<a href="#!" class="waves-effect waves-light">
					Ijin Kerja
					<i class="ti-angle-down"></i>
				</a>
				<ul class="show-notification">
					<li class="waves-effect waves-light"><a href="<?php echo base_url().'permit/kop'; ?>"><i class="ti-file"></i> Personal </a></li>
					<li class="waves-effect waves-light"><a href="<?php echo base_url().'permit/operationalTools'; ?>"><i class="ti-file"></i> Alat </a></li>
					<li class="waves-effect waves-light"><a href="<?php echo base_url().'permit/special'; ?>"><i class="ti-files"></i> Khusus </a></li>
				</ul>
			</li>
			<li class="header-notification">
				<a href="#!" class="waves-effect waves-light">
					Laporan
					<i class="ti-angle-down"></i>
				</a>
				<ul class="show-notification">
					<li class="waves-effect waves-light"><a href="<?php echo base_url().'reporting/shePerformance'; ?>"><i class="ti-file"></i> SHE Performance </a></li>
					<li class="waves-effect waves-light"><a href="<?php echo base_url().'reporting/sheReportAndAnalysis'; ?>"><i class="ti-file"></i> SHE Report &quot; Analysis </a></li>
					<li class="waves-effect waves-light"><a href="<?php echo base_url().'reporting/sheAccountability'; ?>"><i class="ti-files"></i> SHE Acountability </a></li>
				</ul>
			</li>
			<li class="header-notification">
				<a href="#!" class="waves-effect waves-light">
					Master Data
					<i class="ti-angle-down"></i>
				</a>
				<ul class="show-notification">
					<li class="waves-effect waves-light"><a href="#"><i class="ti-file"></i> Lokasi / Area </a></li>
					<li class="waves-effect waves-light"><a href="#"><i class="ti-file"></i> Jenis Ijin Kerja Khusus </a></li>
					<li class="waves-effect waves-light"><a href="#"><i class="ti-file"></i> Jenis Training </a></li>
					<li class="waves-effect waves-light"><a href="#"><i class="ti-file"></i> Perhitungan Resiko </a></li>
					<li class="waves-effect waves-light"><a href="#"><i class="ti-file"></i> Jenis Peraturan </a></li>
					<li class="waves-effect waves-light"><a href="#"><i class="ti-file"></i> Project / Site </a></li>
					<li class="waves-effect waves-light"><a href="#"><i class="ti-file"></i> Alat / Fasilitas </a></li>
					<li class="waves-effect waves-light"><a href="#"><i class="ti-file"></i> Jenis Alat Berat </a></li>
					<li class="waves-effect waves-light"><a href="#"><i class="ti-file"></i> Section </a></li>
				</ul>
			</li>
			<li class="header-notification">
				<a href="#!" class="waves-effect waves-light">
					Manajemen User
					<i class="ti-angle-down"></i>
				</a>
				<ul class="show-notification">
					<li class="waves-effect waves-light"><a href="#"><i class="ti-file"></i> User </a></li>
					<li class="waves-effect waves-light"><a href="#"><i class="ti-file"></i> Karyawan </a></li>
					<li class="waves-effect waves-light"><a href="#"><i class="ti-file"></i> Jabatan </a></li>
					<li class="waves-effect waves-light"><a href="#"><i class="ti-file"></i> Dept / Bagian </a></li>
				</ul>
			</li>
		</ul>
		<ul class="nav-right">
			<?php
			if (!$this->agent->is_mobile()) {
				echo '&nbsp;';
			}else {
			?>
			<li class="header-notification">
				<a href="#!" class="waves-effect waves-light" title="Menu">
					<i class="ti-menu"></i>
				</a>
				<ul class="show-notification">
					<li class="waves-effect waves-light">
						Modul HSE
						<hr />
						<ul>
							<li><a href="<?php echo base_url().'modul/registerPerbaikan'; ?>"><i class="ti-file"></i> Register Tindakan Perbaikan </a></li>
							<li><a href="<?php echo base_url().'modul/hiradc'; ?>"><i class="ti-file"></i> HIRADC </a></li>
							<li><a href="<?php echo base_url().'modul/reportHazard'; ?>"><i class="ti-files"></i> Hazard Report </a></li>
						</ul>
					</li>
					<li class="waves-effect waves-light">
						Ijin Kerja
						<hr />
						<ul>
							<li><a href="<?php echo base_url().'permit/kop'; ?>"><i class="ti-file"></i> Personal </a></li>
							<li><a href="<?php echo base_url().'permit/operationalTools'; ?>"><i class="ti-file"></i> Alat </a></li>
							<li><a href="<?php echo base_url().'permit/special'; ?>"><i class="ti-files"></i> Khusus </a></li>
						</ul>
					</li>
					<li class="waves-effect waves-light">
						Laporan
						<hr />
						<ul>
							<li><a href="<?php echo base_url().'reporting/shePerformance'; ?>"><i class="ti-file"></i> SHE Performance </a></li>
							<li><a href="<?php echo base_url().'reporting/sheReportAndAnalysis'; ?>"><i class="ti-file"></i> SHE Report &quot; Analysis </a></li>
							<li><a href="<?php echo base_url().'reporting/sheAccountability'; ?>"><i class="ti-files"></i> SHE Acountability </a></li>
						</ul>
					</li>
				</ul>
			</li>
			<?php }?>
			<li class="user-profile header-notification">
				<a href="#!" class="waves-effect waves-light">
					<img src="<?php echo base_url();?>assets/images/avatar-1.jpg" class="img-radius" alt="User-Profile-Image">
					<span>John Doe</span>
					<i class="ti-angle-down"></i>
				</a>
				<ul class="show-notification profile-notification">
					<li class="waves-effect waves-light">
						<a href="auth-normal-sign-in.html">
							<i class="ti-layout-sidebar-left"></i> Logout
						</a>
					</li>
				</ul>
			</li>			
		</ul>
	</div>

</div>