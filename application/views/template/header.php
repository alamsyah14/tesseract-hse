<!DOCTYPE html>
<html>
<head>
  	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
  	<meta http-equiv="X-UA-Compatible" content="IE=edge">
  	<meta http-equiv="X-UA-Compatible" content1="IE=Edge">
    <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE9">
	  <meta http-equiv="X-UA-Compatible" content="IE=9" />
    <link rel="icon" href="<?php echo base_url();?>assets/images/hse-single-logo.ico" type="image/x-icon">
	  <title>TMS HSE Monitoring</title>
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,500" rel="stylesheet">
  	<link rel="stylesheet" href="<?php echo base_url();?>assets/pages/waves/css/waves.min.css" type="text/css" media="all">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap/css/bootstrap.min.css" type="text/css">   
    <link rel="stylesheet" href="<?php echo base_url();?>assets/pages/waves/css/waves.min.css" type="text/css" media="all">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/icon/themify-icons/themify-icons.css" type="text/css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/icon/font-awesome/css/font-awesome.min.css" type="text/css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/jquery.mCustomScrollbar.css" type="text/css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/sweet-alert/sweetalert2.min.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/style.css" type="text/css">
</head>