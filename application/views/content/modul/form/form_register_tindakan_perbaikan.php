<!-- Page-header start -->
<div class="page-header">&nbsp;</div>
<!-- Page-header end -->
<div class="pcoded-inner-content">
	<div class="main-body">
		<div class="page-wrapper">
			<div class="page-body">
				<div class="row">
					
					<div class="col-sm-12" style="padding-left: 0px;padding-right: 0px;">
						<div class="card">
							<div class="card-block">
								<div class="row align-items-center">
									<div class="col-md-12">
										<div class="page-header-title">
				                          <h5 class="m-b-10">Modul HSE</h5>
				                          <p class="m-b-0"><?php echo $type;?> Register Tindakan Perbaikan</p>
				                      </div>
									</div>
								</div>
							</div>
							<div class="card-header">&nbsp;</div>
							<div class="card-block">
								<form role="form" enctype="multipart/form-data" id="form_">
									<div class="form-group row">
                                        <label class="col-sm-2 col-form-label">No. Register</label>
                                        <div class="col-sm-10">
                                            <input id="register_no" name="register_no" type="text" class="form-control" readonly />
                                            <input id="form_type" name="form_type" type="hidden" class="form-control" readonly />
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                    	<label class="col-sm-2 col-form-label">Tipe</label>
                                    	<div class="col-sm-10">
                                    		<select id="tipe_" name="tipe_" class="form-control">
                                    			<option value="null"> -- PILIH -- </option>
                                    		</select>
                                    	</div>
                                    </div>
                                    <div class="form-group row">
                                    	<label class="col-sm-2 col-form-label">Lokasi / Area</label>
                                    	<div class="col-sm-10">
                                    		<select id="lokasi_" name="lokasi_" class="form-control">
                                    			<option value="null"> -- PILIH -- </option>
                                    		</select>
                                    	</div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-2 col-form-label">Referensi</label>
                                        <div class="col-sm-10">
                                            <input id="referensi_" name="referensi_" type="text" class="form-control" />
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-2 col-form-label">No. Referensi</label>
                                        <div class="col-sm-10">
                                            <input id="referensi_no" name="referensi_no" type="text" class="form-control" />
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-2 col-form-label">Rekomendasi Tindakan Perbaikan</label>
                                        <div class="col-sm-10">
                                        	<textarea rows="5" id="rekomendasi_" name="rekomendasi_" class="form-control"></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                    	<label class="col-sm-2 col-form-label">Penanggungjawab</label>
                                    	<div class="col-sm-10">
                                    		<select id="penangung_jawab_" name="penangung_jawab_" class="form-control">
                                    			<option value="null"> -- PILIH -- </option>
                                    		</select>
                                    	</div>
                                    </div>
                                    <div class="form-group row">
                                    	<label class="col-sm-2 col-form-label">Penanggungjawab email</label>
                                    	<div class="col-sm-10">
                                    		<input id="penangung_jawab_email_" name="penangung_jawab_email_" type="text" class="form-control" />
                                    	</div>
                                    </div>

                                    <div class="form-group row">
                                    	<label class="col-sm-2 col-form-label">Tanggal Pelaksanaan</label>
                                    	<div class="col-sm-10">
                                    		<div class="input-group">
                                    			<span class="input-group-text" style="padding-right: 5px;"><i class="fa fa-calendar f-30"></i></span>
                                    			<input id="tanggal_pelaksanaan_" name="tanggal_pelaksanaan_" type="text" class="form-control datepickerForm" readonly />
                                    		</div>                                    		
                                    	</div>
                                    </div>
                                    <div class="form-group row">
                                    	<label class="col-sm-2 col-form-label">Status</label>
                                    	<div class="col-sm-10">
                                    		<select id="status_" name="status_" class="form-control">
                                    			<option value="null"> -- PILIH -- </option>
                                    			<option value="open"> OPEN </option>
                                    			<option value="closed"> CLOSED </option>
                                    		</select>
                                    	</div>
                                    </div>



								</fom>
								
							</div>
							<div class="card-footer row">
								<div align="left" class="col-sm-6">
									<button id="button_back" onclick="backto_list()" type="button" class="btn btn-danger waves-effect waves-light">Kembali</button>
								</div>
								<div align="right" class="col-sm-6">
									<button id="button_process" onclick="process()" type="button" class="btn btn-primary waves-effect waves-light">Simpan</button>
									<button id="button_reset" onclick="reset()" type="button" class="btn btn-default waves-effect waves-light">Mengatur ulang</button>
								</div>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>
</div>
<script>
	function backto_list() {
		var url                 = '<?php echo base_url()?>modul/registerPerbaikan';   
    	window.location.href    = url;
	}

	function process() {

	}

	function reset() {
		 document.getElementById("form_").reset(); 
	}
</script>
