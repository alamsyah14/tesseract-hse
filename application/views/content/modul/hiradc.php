<!-- Page-header start -->
<div class="page-header">&nbsp;</div>
<!-- Page-header end -->

<div class="pcoded-inner-content">
  <div class="main-body">
    <div class="page-wrapper">
      <div class="page-body">
        <div class="row">
          <!-- Page-header start -->
          <div class="col-sm-12" style="padding-left: 0px;padding-right: 0px;">
            <div class="card">
              <div class="card-block">
                <div class="row align-items-center">
                  <div class="col-md-12">
                      <div class="page-header-title">
                          <h5 class="m-b-10">Modul HSE</h5>
                          <p class="m-b-0">Hazard Identification Risk Assessment Determining Control (HIRADC)</p>
                      </div>
                  </div>
                  <hr />
                  <div class="col-md-12">
                    
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!-- Page-header end -->
          
          <!-- Page-body start -->
          <div class="col-sm-12" style="padding-left: 0px;padding-right: 0px;">
            <div class="card">
              <div class="card-header">
                &nbsp;
                <div class="card-header-right">
                  <ul class="list-unstyled card-option">
                    <li><i class="fa fa fa-wrench open-card-option"></i></li>
                    <li><i class="fa fa-window-maximize full-card"></i></li>
                    <li><i class="fa fa-minus minimize-card"></i></li>
                    <li onclick="getDataTables()"><i class="fa fa-refresh"></i></li>
                  </ul>
                </div>
              </div>
              <div class="card-block table-border-style" style="">
                <div class="table-responsive">
                  <table id="table_data" class="table table-bordered table-hover dataTable dtr-inline" role="grid" style="width:100%">
                    <thead>
                      <tr>
                        <th><div align="center">No.</div></th>
                        <th><div align="left">Project/Site</div></th>
                        <th><div align="left">Lokasi/Area</div></th>
                        <th><div align="left">Section</div></th>
                        <th><div align="center">Period</div></th>
                        <th><div align="center">Action</div></th>
                      </tr>
                    </thead>
                    <tbody id="show_data"></tbody>
                  </table>
                </div>                
              </div>
            </div>            
          </div>
          <!-- Page-body end -->

        </div>
      </div>  
    </div>  
  </div>
</div>
<script>
  
  var table_ = $('#table_data').DataTable({
      "lengthMenu": [[10, 50, 100, 150, 200, -1], [10, 50, 100, 150, 200, "All"]],
      "AutoWidth": true,
      "select": true,
      "fixedColumns":true,
      "ordering": true,
      "searching": false   
  });

  window.onload = function (){ 
    getDataTables();         
  };

  function getDataTables() 
  {
    $('#show_data').html('<tr><td colspan="6" align="center"><img src="<?php echo base_url('assets/loading/ajax-loader.gif') ?>" border="0" height=15 width=15 /> &nbsp; Loading ...</td></tr>');
    $.ajax({
      url: '<?php echo base_url('moduls/get_data_hiradc');?>',
      type: 'POST',
      dataType: 'json',
      success: function(respon){
        if(respon.status == true)
        {
          table_.clear();
          if(respon.data.length > 0)
          {
            for(i=0; i<respon.data.length; i++){
              table_.row.add(respon.data[i]).draw(false);               
            }
          }
          else
          {
            $('#show_data').html('<tr><td align="center" colspan="6" class="dataTables_empty" valign="top">No data available in table</td></tr>');
          }
        }
        else
        {
          table_.clear();
          $('#show_data').html('<tr><td align="center" colspan="6" class="dataTables_empty" valign="top">No data available in table</td></tr>');
        }
      },
      error: function(){
        table_.clear();
        $('#show_data').html('<tr><td align="center" colspan="6" class="dataTables_empty" valign="top">No data available in table</td></tr>');
      }
    });
  }

</script>