<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller 
{

	function __construct() 
    {
		parent::__construct();
		$this->load->library('session');
	}

	function validate_login(){}

	function logout()
	{
		$this->session->sess_destroy();		
		echo json_encode(array('status'=>true,'message'=>''));
	}

}