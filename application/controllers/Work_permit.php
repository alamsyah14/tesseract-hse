<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Work_permit extends CI_Controller 
{
	function __construct() 
    {
		parent::__construct();
		//$this->load->library('session');
		//$sess_id  = $this->session->userdata('user_id');
		//if(IsNullOrEmptyString($sess_id)){
		//	echo '<script>window.location.href="'.base_url().'nosession";</script>';        
		//}
		$this->load->model('Work_permit_model','moduls');
	}

	public function kop() 
	{
		$data = array (
    		'isi' => 'content/ijin_kerja/ijin_kerja_kop'
    	);
		$this->load->view('template',$data);	
	}

	public function operational_alat() 
	{
		$data = array (
    		'isi' => 'content/ijin_kerja/tools_operational'
    	);
		$this->load->view('template',$data);	
	}

	public function khusus() 
	{
		$data = array (
    		'isi' => 'content/ijin_kerja/special'
    	);
		$this->load->view('template',$data);	
	}


	function get_data_kop() 
	{
		list($result,$data) = $this->moduls->getDataKOP();
		if($result === true)
		{
			$no     = 1;			
			foreach($data as $row => $value)
			{				
				$list_data[] = array(
					'<div align="center">'.$no.'.</div>',
					'<div align="center"> '.$value['_nik'].'</div>',
					'<div align="left"> '.$value['_nama'].'</div>',
					'<div align="left"> '.$value['_jabatan'].'</div>',
					'<div align="left"> '.$value['_email'].'</div>',
					'<div align="center"> '.$value['_approval'].'</div>',
					'<div align="center"> # </div>',					 
				);
				$no++;	
			}
			echo json_encode(array('status'=>true,'data'=>$list_data));			
		}
		else { echo json_encode(array('status'=>false,'message'=>ERROR_GLOBAL)); }
	}

	function get_data_operational_alat() 
	{
		list($result,$data) = $this->moduls->getDataOperationalTools();
		if($result === true)
		{
			$no     = 1;			
			foreach($data as $row => $value)
			{				
				$list_data[] = array(
					'<div align="center">'.$no.'.</div>',
					'<div align="center"> '.$value['_number_permit'].'</div>',
					'<div align="left"> '.$value['_nama'].'</div>',
					'<div align="left"> '.$value['_merk'].'</div>',
					'<div align="center"> '.$value['_capacity'].'</div>',
					'<div align="center"> # </div>',					 
				);
				$no++;	
			}
			echo json_encode(array('status'=>true,'data'=>$list_data));			
		}
		else { echo json_encode(array('status'=>false,'message'=>ERROR_GLOBAL)); }	
	}

	function get_data_khusus() 
	{
		list($result,$data) = $this->moduls->getDataSpecialPermit();
		if($result === true)
		{
			$no     = 1;			
			foreach($data as $row => $value)
			{				
				$list_data[] = array(
					'<div align="center">'.$no.'.</div>',
					'<div align="center"> '.$value['_number_permit'].'</div>',
					'<div align="center"> '.$value['_type'].'</div>',
					'<div align="center"> '.$value['_start_date'].'</div>',
					'<div align="center"> '.$value['_end_date'].'</div>',
					'<div align="left"> '.$value['_location'].'</div>',
					'<div align="center"> '.$value['_status'].'</div>',
					'<div align="center"> # </div>',					 
				);
				$no++;	
			}
			echo json_encode(array('status'=>true,'data'=>$list_data));			
		}
		else { echo json_encode(array('status'=>false,'message'=>ERROR_GLOBAL)); }
		
	}
}