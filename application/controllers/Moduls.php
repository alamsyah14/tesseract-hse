<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Moduls extends CI_Controller 
{
	function __construct() 
    {
		parent::__construct();
		//$this->load->library('session');
		//$sess_id  = $this->session->userdata('user_id');
		//if(IsNullOrEmptyString($sess_id)){
		//	echo '<script>window.location.href="'.base_url().'nosession";</script>';        
		//}
		$this->load->model('Moduls_model','moduls');
	}

	public function modul_regist_perbaikan() 
	{
		$data = array (
    		'isi' => 'content/modul/register_tindakan_perbaikan'
    	);
		$this->load->view('template',$data);	
	}

	public function modul_hiradc() 
	{
		$data = array (
    		'isi' => 'content/modul/hiradc'
    	);
		$this->load->view('template',$data);	
	}

	public function modul_hazard_report() 
	{
		$data = array (
    		'isi' => 'content/modul/hazard_report'
    	);
		$this->load->view('template',$data);	
	}

	function form_regist_perbaikan() 
	{
		$_type 		= $this->uri->segment(3);
		$rows_id 	= $this->uri->segment(4);
		
		$data = array (
    		'type' 		=> $_type,
    		'rows_id'	=> $rows_id,
    		'isi' 		=> 'content/modul/form/form_register_tindakan_perbaikan'
    	);

		$this->load->view('template',$data);	
	}


	function get_data_register_perbaikan() 
	{
		list($result,$data) = $this->moduls->getDataRTP('admin');
		if($result === true)
		{
			$no     = 1; $edit = 'Ubah'; $detail = 'Detail';	
			foreach($data as $row => $value)
			{				
				$list_data[] = array(
					'<div align="center">'.$no.'.</div>',
					'<div align="left"> '.$value['_type'].'</div>',
					'<div align="left"> '.$value['_lokasi'].'</div>',
					'<div align="left"> '.$value['_penanggung_jawab'].'</div>',
					'<div align="center"> '.$value['_verifikasi'].'</div>',
					'<div align="left"> '.$value['_referensi'].'</div>',
					'<div align="center"> '.$value['_status'].'</div>',
					'<div align="center">
						<button type="button" class="btn waves-effect waves-light btn-info btn-outline-info" onclick="goto(\''.$detail.'\',\''.$value['_id'].'\')">Detail</button>
						<button type="button" class="btn waves-effect waves-light btn-inverse btn-outline-inverse" onclick="goto(\''.$edit.'\',\''.$value['_id'].'\')">Ubah</button>
					</div>',					 
				);
				$no++;	
			}
			echo json_encode(array('status'=>true,'data'=>$list_data));			
		}
		else { echo json_encode(array('status'=>false,'message'=>ERROR_GLOBAL)); }
	}

	function get_data_hiradc() 
	{
		list($result,$data) = $this->moduls->getDataHIRADC();
		if($result === true)
		{
			$no     = 1;			
			foreach($data as $row => $value)
			{				
				$list_data[] = array(
					'<div align="center">'.$no.'.</div>',
					'<div align="left"> '.$value['_project_name'].'</div>',
					'<div align="left"> '.$value['_lokasi'].'</div>',
					'<div align="left"> '.$value['_section_nama'].'</div>',
					'<div align="left"> '.$value['_periode'].'</div>',
					'<div align="center"> # </div>',					 
				);
				$no++;	
			}
			echo json_encode(array('status'=>true,'data'=>$list_data));			
		}
		else { echo json_encode(array('status'=>false,'message'=>ERROR_GLOBAL)); }
		
	}

	function get_data_hazard_report() 
	{
		list($result,$data) = $this->moduls->getDataHazardReport();
		if($result === true)
		{
			$no     = 1;			
			foreach($data as $row => $value)
			{				
				$list_data[] = array(
					'<div align="center">'.$no.'.</div>',
					'<div align="center"> '.$value['_number_hazard'].'</div>',
					'<div align="center"> '.$value['_tanggal_temuan'].'</div>',
					'<div align="left"> '.$value['_jenis_bahaya'].'</div>',
					'<div align="center"> # </div>',					 
				);
				$no++;	
			}
			echo json_encode(array('status'=>true,'data'=>$list_data));			
		}
		else { echo json_encode(array('status'=>false,'message'=>ERROR_GLOBAL)); }
		
	}


}