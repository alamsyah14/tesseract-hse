<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Report extends CI_Controller 
{
	function __construct() 
    {
		parent::__construct();
		//$this->load->library('session');
		//$sess_id  = $this->session->userdata('user_id');
		//if(IsNullOrEmptyString($sess_id)){
		//	echo '<script>window.location.href="'.base_url().'nosession";</script>';        
		//}
		//$this->load->model('Moduls_model','moduls');
	}

	public function she_performance() 
	{
		$data = array (
    		'isi' => 'content/laporan/she_performance'
    	);
		$this->load->view('template',$data);	
	}

	public function she_report_and_analysis() 
	{
		$data = array (
    		'isi' => 'content/laporan/she_report_and_analysis'
    	);
		$this->load->view('template',$data);	
	}

	public function she_accountability() 
	{
		$data = array (
    		'isi' => 'content/laporan/she_accountability'
    	);
		$this->load->view('template',$data);	
	}
}