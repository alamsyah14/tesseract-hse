<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller 
{

	function __construct() 
    {
		parent::__construct();
		//$this->load->library('session');
		//$sess_id  = $this->session->userdata('user_id');
		//if(IsNullOrEmptyString($sess_id)){
		//	echo '<script>window.location.href="'.base_url().'nosession";</script>';        
		//}
		//$this->load->model('Query_model','m_query');
	}

	public function index()
    {
    	$data = array (
    		'isi' => 'content/dashboard/home_pages'
    	);
		$this->load->view('template',$data);	
	}


	
}