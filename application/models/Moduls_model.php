<?php

class Moduls_model extends CI_Model
{
    public function getNoRTP()
    {
        $lastId = $this->db->table('tms_rtp')
            ->select('MAX(RIGHT(no_rtp,4)) AS kd_max')
            ->where('DATE(created_at)= CURDATE()')
            ->orderBy('id', 'DESC')
            ->limit(1)
            ->get()->getRowArray();

        if ($lastId) {
            $tmp = ((int)$lastId['kd_max']) + 1;
            $kd = sprintf("%04s", $tmp);
        } else {
            $kd = "0001";
        }
        date_default_timezone_set('Asia/Jakarta');
        return "RTP" . date('Ymd') . "-" . $kd;
    }

    public function getNoHazard()
    {
        $lastId = $this->db->table('tms_hazard')
            ->select('MAX(RIGHT(no_hazard,4)) AS kd_max')
            ->where('DATE(created_at)= CURDATE()')
            ->orderBy('id', 'DESC')
            ->limit(1)
            ->get()->getRowArray();


        if ($lastId) {
            $tmp = ((int)$lastId['kd_max']) + 1;
            $kd = sprintf("%04s", $tmp);
        } else {
            $kd = "0001";
        }
        date_default_timezone_set('Asia/Jakarta');
        return "HZD" . date('Ymd') . "-" . $kd;
    }

    function getDataRTP($roles,$idEmployee = '')
    {
    	$where = "";$result = false; $rows = array();
    	if ($roles != 'admin' && $roles != 'safety') 
    	{
    		$where = " AND tms_rtp.penanggung_jawab = '$idEmployee'";
    	}

    	$sql = "SELECT tms_rtp.id,tms_rtp.type,md_lokasi.nama as lokasi_id,md_karyawan.nama_lengkap as penanggung_jawab,verifikasi,referensi,status 
    			FROM tms_rtp 
    			INNER JOIN md_lokasi ON md_lokasi.id = tms_rtp.lokasi_id
    			LEFT JOIN md_karyawan ON md_karyawan.id = tms_rtp.penanggung_jawab
    			WHERE tms_rtp.deleted_at IS NULL ".$where."
    			ORDER BY tms_rtp.id DESC";

    	$que = $this->db->query($sql);
        $num = $que->num_rows();

        if ($num > 0) {
            foreach ($que->result() as $row) {
                $rows[] = array(
                    '_id'         		=> $row->id,
                    '_type'       		=> $row->type,
                    '_lokasi'  			=> $row->lokasi_id,
                    '_penanggung_jawab' => $row->penanggung_jawab,
                    '_verifikasi'  		=> $row->verifikasi,
                    '_referensi'   		=> $row->referensi,
                    '_status'     		=> $row->status
                );
            }

            $result = true;
        }

        return array($result,$rows); 
    }

    function getDataHIRADC() 
    {
    	$result = false; $rows = array();

    	$sql = "SELECT tms_hiradc.id,md_project.nama as namaprojek,md_lokasi.nama as lokasi_id,md_section.nama as section_nama,tms_hiradc.periode
    			FROM tms_hiradc
    			INNER JOIN md_lokasi ON md_lokasi.id = tms_hiradc.lokasi_id
    			INNER JOIN md_project ON md_project.id = tms_hiradc.project_id
    			INNER JOIN md_section ON md_section.id = tms_hiradc.section
    			WHERE tms_hiradc.deleted_at IS NULL
    			ORDER BY tms_hiradc.id DESC";

    	$que = $this->db->query($sql);
        $num = $que->num_rows();

        if ($num > 0) {
            foreach ($que->result() as $row) {
                $rows[] = array(
                    '_id'         		=> $row->id,
                    '_project_name'     => $row->namaprojek,
                    '_lokasi'  			=> $row->lokasi_id,
                    '_section_nama' 	=> $row->section_nama,
                    '_periode'  		=> $row->periode
                );
            }

            $result = true;
        }

        return array($result,$rows);
    }

    function getDataHazardReport() 
    {
    	$result = false; $rows = array();

    	$sql = "SELECT tms_hazard.id,tms_hazard.no_hazard,CONCAT(hari_temuan, ', ', tanggal_temuan,' ',jam_laporan) AS tanggal_temuan,jenis_bahaya
    			FROM tms_hazard
    			WHERE tms_hazard.deleted_at IS NULL
    			ORDER BY tms_hazard.id DESC";

    	$que = $this->db->query($sql);
        $num = $que->num_rows();

        if ($num > 0) {
            foreach ($que->result() as $row) {
                $rows[] = array(
                    '_id'         		=> $row->id,
                    '_number_hazard'    => $row->no_hazard,
                    '_tanggal_temuan'  	=> $row->tanggal_temuan,
                    '_jenis_bahaya' 	=> $row->jenis_bahaya
                );
            }

            $result = true;
        }

        return array($result,$rows);
    }
    
}