<?php

class Work_permit_model extends CI_Model
{

	function getDataKOP()
    {
    	$result = false; $rows = array();
    	$sql = "SELECT tms_ijin_kerja_personal.id,ikp_nik,ikp_nama,ikp_jabatan,ikp_email,approval_hse 
    			FROM tms_ijin_kerja_personal 
    			WHERE tms_ijin_kerja_personal.deleted_at IS NULL
    			ORDER BY tms_ijin_kerja_personal.id DESC";

    	$que = $this->db->query($sql);
        $num = $que->num_rows();

        if ($num > 0) {
            foreach ($que->result() as $row) {
                $rows[] = array(
                    '_id'         	=> $row->id,
                    '_nik'       	=> $row->ikp_nik,
                    '_nama'  		=> $row->ikp_nama,
                    '_jabatan' 		=> $row->ikp_jabatan,
                    '_email'  		=> $row->ikp_email,
                    '_approval'   	=> $row->approval_hse
                );
            }

            $result = true;
        }

        return array($result,$rows); 
    }

    function getDataOperationalTools()
    {
    	$result = false; $rows = array();
    	$sql = "SELECT tms_ijin_operasional_alat_berat.id,no_ijin_operasional,nama_kontraktor,merk_manufacturer,kapasitas_class 
    			FROM tms_ijin_operasional_alat_berat 
    			WHERE tms_ijin_operasional_alat_berat.deleted_at IS NULL
    			ORDER BY tms_ijin_operasional_alat_berat.id DESC";

    	$que = $this->db->query($sql);
        $num = $que->num_rows();

        if ($num > 0) {
            foreach ($que->result() as $row) {
                $rows[] = array(
                    '_id'         		=> $row->id,
                    '_number_permit'    => $row->no_ijin_operasional,
                    '_nama'  			=> $row->nama_kontraktor,
                    '_merk' 			=> $row->merk_manufacturer,
                    '_capacity'  		=> $row->kapasitas_class
                );
            }

            $result = true;
        }

        return array($result,$rows); 
    }

    function getDataSpecialPermit()
    {
    	$result = false; $rows = array();
    	$sql = "SELECT tms_ijin_kerja_khusus.id,nomor_ijin,jenis_ijinkerja_khusus,tgl_mulai,tgl_selesai,md_lokasi.nama as lokasi,status 
    			FROM tms_ijin_kerja_khusus 
    			INNER JOIN md_lokasi ON md_lokasi.id = tms_ijin_kerja_khusus.lokasi_id
    			WHERE tms_ijin_kerja_khusus.deleted_at IS NULL
    			ORDER BY tms_ijin_kerja_khusus.id DESC";

    	$que = $this->db->query($sql);
        $num = $que->num_rows();

        if ($num > 0) {
            foreach ($que->result() as $row) {
                $rows[] = array(
                    '_id'         		=> $row->id,
                    '_number_permit'    => $row->nomor_ijin,
                    '_type'  			=> $row->jenis_ijinkerja_khusus,
                    '_start_date' 		=> $row->tgl_mulai,
                    '_end_date' 		=> $row->tgl_selesai,
                    '_location'  		=> $row->lokasi,
                    '_status'  			=> $row->status
                );
            }

            $result = true;
        }

        return array($result,$rows); 
    }

}